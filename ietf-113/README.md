# Presenting Unilateral Probing at IETF 113

This WG adopted draft will be presented at the DPRIVE session at IETF 113.

This directory contains the source for slides at that session.

To convert the source to a slide deck, you need [darkslide](https://github.com/ionelmc/python-darkslide) installed.
From this directory, run `make`
The default output is an HTML5 slide deck.

If you want a PDF variant, there are two choices:

- install the proprietary [Prince](https://www.princexml.com/) software, or
- open the html output in chromium or firefox, hit `esc` to expose all slides, choose "Print", and print to pdf.
