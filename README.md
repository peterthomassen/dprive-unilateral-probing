Unilateral probing for recursor-to-authoritative DNS
====================================================

This repository hosts then source for [draft-ietf-dprive-unilateral-probing](https://datatracker.ietf.org/doc/draft-ietf-dprive-unilateral-probing/).

You can see the editor's copies (not yet submitted to the datatracker) online in different formats ([html](https://dkg.gitlab.io/dprive-unilateral-probing/), [pdf](https://dkg.gitlab.io/dprive-unilateral-probing/unilateral-probing.pdf), or [text](https://dkg.gitlab.io/dprive-unilateral-probing/unilateral-probing.txt)).
